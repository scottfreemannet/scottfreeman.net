import React from "react";
import "semantic-ui-css/semantic.min.css";
import { Grid, Container, Header, List, Icon } from "semantic-ui-react";
import "./App.css";

function App() {
  return (
    <div>
      <Grid verticalAlign="middle" className="full-height">
        <Grid.Row stretched>
          <Grid.Column tablet={4} computer={8} only="tablet computer"></Grid.Column>
          <Grid.Column mobile={16} tablet={12} computer={8} className="overlay element-animation">
            <Container>
              <Header as="h1">
                Hello. I'm <strong>Scott Freeman</strong>.
              </Header>
              <p>
                I'm a front-end web developer with skills in JS, React, Sass, and various other frameworks and build
                systems.
              </p>
              <p>In my spare time I enjoy the outdoors, keeping fit, and walking my dog.</p>
              <List horizontal>
                <List.Item>
                  <a href="https://facebook.com/scottfreeman.net">
                    <Icon link circular inverted color="blue" size="big" name="facebook" />
                  </a>
                </List.Item>
                <List.Item>
                  <a href="https://www.instagram.com/scottfreeman/">
                    <Icon link circular inverted color="orange" size="big" name="instagram" />
                  </a>
                </List.Item>
                <List.Item>
                  <a href="linkedin.com/in/scottfreeman">
                    <Icon link circular inverted color="blue" size="big" name="linkedin" />
                  </a>
                </List.Item>
                <List.Item>
                  <a href="https://gitlab.com/scottfreeman">
                    <Icon link circular inverted color="black" size="big" name="gitlab" />
                  </a>
                </List.Item>
                <List.Item>
                  <a href="mailto:hello@scottfreeman.net">
                    <Icon link circular inverted color="grey" size="big" name="mail" />
                  </a>
                </List.Item>
                <List.Item>
                  <a href="http://m.me/scottfreeman.net">
                    <Icon link circular inverted color="olive" size="big" name="chat" />
                  </a>
                </List.Item>
              </List>
            </Container>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
}

export default App;
